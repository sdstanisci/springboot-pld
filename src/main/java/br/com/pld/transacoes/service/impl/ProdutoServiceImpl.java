package br.com.pld.transacoes.service.impl;

import java.util.List;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import br.com.pld.transacoes.domain.entity.Produto;
import br.com.pld.transacoes.domain.repository.ProdutoRepository;
import br.com.pld.transacoes.exception.RegraNegocioException;
import br.com.pld.transacoes.service.ProdutoService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class ProdutoServiceImpl implements ProdutoService {

	private final ProdutoRepository produtoRepository;

	@Override
	public Produto getProdutoById(Integer id) {

		return produtoRepository.findById(id)
				.orElseThrow(() -> new RegraNegocioException("Produto não encontrado."));
	}

	@Override
	public Produto save(Produto produto) {

		return produtoRepository.save(produto);
	}

	@Override
	public void delete(Integer id) {

		produtoRepository.findById(id).map(produto -> {
			produtoRepository.delete(produto);
			return produto;
		}).orElseThrow(() -> new RegraNegocioException("Problema ao deletar o produto, não encontrado."));

	}

	@Override
	public void update(Integer id, Produto produto) {

		produtoRepository.findById(id).map(produtoEncontrado -> {
			produto.setId(produtoEncontrado.getId());
			produtoRepository.save(produto);
			return produto;
		}).orElseThrow(() -> new RegraNegocioException("Problema ao atualizar o produto, não encontrado."));
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Produto> findFilter(Produto filtro) {

		ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase()
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

		Example example = Example.of(filtro, matcher);

		return produtoRepository.findAll(example);
	}
}
