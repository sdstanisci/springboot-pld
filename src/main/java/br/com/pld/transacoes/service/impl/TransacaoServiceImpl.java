package br.com.pld.transacoes.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import br.com.pld.transacoes.domain.entity.Cliente;
import br.com.pld.transacoes.domain.entity.ItemTransacao;
import br.com.pld.transacoes.domain.entity.Produto;
import br.com.pld.transacoes.domain.entity.Transacao;
import br.com.pld.transacoes.domain.enums.StatusTransacao;
import br.com.pld.transacoes.domain.repository.ClienteRepository;
import br.com.pld.transacoes.domain.repository.ItemTransacaoRepository;
import br.com.pld.transacoes.domain.repository.ProdutoRepository;
import br.com.pld.transacoes.domain.repository.TransacaoRepository;
import br.com.pld.transacoes.exception.RegraNegocioException;
import br.com.pld.transacoes.exception.TransacaoNaoEncontradoException;
import br.com.pld.transacoes.rest.dto.ItemTransacaoDTO;
import br.com.pld.transacoes.rest.dto.TransacaoDTO;
import br.com.pld.transacoes.service.TransacaoService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class TransacaoServiceImpl implements TransacaoService{

	private final TransacaoRepository transacaoRepository;
	private final ClienteRepository clienteRepository;
	private final ProdutoRepository produtoRepository;
	private final ItemTransacaoRepository itemTransacaoRepository;
	
	@Override
	@Transactional
	public Transacao salvar(TransacaoDTO dto) {
		
		Integer idCliente = dto.getCliente();
		
		Cliente cliente = clienteRepository.findById(idCliente)
			.orElseThrow( () -> new RegraNegocioException("Código Cliente inválido !"));
		
		Transacao transacao = new Transacao();
		transacao.setTotal(dto.getTotal());
		transacao.setDataTransacao(LocalDate.now());
		transacao.setCliente(cliente);
		transacao.setStatus(StatusTransacao.REALIZADO);
		
		List<ItemTransacao> itensTransacao = converterItens(transacao, dto.getItens());
		
		transacaoRepository.save(transacao);
		
		itemTransacaoRepository.saveAll(itensTransacao);
		
		transacao.setItens(itensTransacao);
		
		return transacao;
	}
	
	private List<ItemTransacao> converterItens(Transacao transacao, List<ItemTransacaoDTO> itens) {
		
		if(itens.isEmpty()) {
			throw new RegraNegocioException("Lista de intens vazia.");
		}
		
		return itens
				.stream()
				.map( dto -> {
					
					Integer idProduto = dto.getProduto();
					Produto produto = produtoRepository
										.findById(idProduto)
										.orElseThrow(() -> new RegraNegocioException("Código do Produto " + idProduto + " Inválido !"));
					
					ItemTransacao itemTransacao = new ItemTransacao();
					
					itemTransacao.setQuantidade(dto.getQuantidade());
					itemTransacao.setTransacao(transacao);		
					itemTransacao.setProduto(produto);
					
					return itemTransacao;
					
				}).collect(Collectors.toList());
		
	}

	@Override
	public Optional<Transacao> obterTransacaoCompleto(Integer id) {
		return transacaoRepository.findByIdFetchItens(id);
	}

	@Override
	@Transactional
	public void atualizaStatus(Integer id, StatusTransacao statusTransacao) {
		
		transacaoRepository
			.findById(id)
			.map(transacao -> {
				transacao.setStatus(statusTransacao);
				return transacaoRepository.save(transacao);
			}).orElseThrow(() -> new TransacaoNaoEncontradoException());
	}
}
