package br.com.pld.transacoes.service;

import java.util.Optional;

import br.com.pld.transacoes.domain.entity.Transacao;
import br.com.pld.transacoes.domain.enums.StatusTransacao;
import br.com.pld.transacoes.rest.dto.TransacaoDTO;

public interface TransacaoService {

	Transacao salvar(TransacaoDTO dto);
	
	Optional<Transacao> obterTransacaoCompleto(Integer id);
	
	void atualizaStatus(Integer id, StatusTransacao statusTransacao);
}
