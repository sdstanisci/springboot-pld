package br.com.pld.transacoes.service;

import java.util.List;

import br.com.pld.transacoes.domain.entity.Cliente;

public interface ClienteService {
	
	Cliente getClienteById(Integer id);
	
	Cliente save(Cliente cliente);
	
	void delete(Integer id);
	
	void update(Integer id, Cliente cliente);
	
	List<Cliente> findFilter(Cliente filtro);
}
