package br.com.pld.transacoes.service.impl;

import java.util.List;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import br.com.pld.transacoes.domain.entity.Cliente;
import br.com.pld.transacoes.domain.repository.ClienteRepository;
import br.com.pld.transacoes.exception.RegraNegocioException;
import br.com.pld.transacoes.service.ClienteService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class ClienteServiceImpl implements ClienteService {

	private final ClienteRepository clienteRepository;

	@Override
	public Cliente getClienteById(Integer id) {

		return clienteRepository.findById(id).map(cliente -> {
			return cliente;
		}).orElseThrow(() -> new RegraNegocioException("Cliente não encontrado !"));
	}

	@Override
	public Cliente save(Cliente cliente) {
		return clienteRepository.save(cliente);
	}

	@Override
	public void delete(Integer id) {
		clienteRepository
			.findById(id)
			.map(cliente -> {
				clienteRepository.delete(cliente);
			return cliente;
			}).orElseThrow(
					() -> new RegraNegocioException("Problema ao deletar o cliente, não encontrado !")
			);
	}

	@Override
	public void update(Integer id, Cliente cliente) {

		clienteRepository
			.findById(id)
			.map(clienteEncontrado -> {
				cliente.setId(clienteEncontrado.getId());
				clienteRepository.save(cliente);
			return clienteEncontrado;
			}).orElseThrow(
					() -> new RegraNegocioException("Problema ao atualizar o cliente, não encontrado !")
			);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Cliente> findFilter(Cliente filtro) {

		ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase()
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

		Example example = Example.of(filtro, matcher);

		return clienteRepository.findAll(example);
	}

}
