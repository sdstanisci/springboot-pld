package br.com.pld.transacoes.service;

import java.util.List;
import br.com.pld.transacoes.domain.entity.Produto;
public interface ProdutoService {

	Produto getProdutoById(Integer id);
	
	Produto save(Produto produto);
	
	void delete(Integer id);
	
	void update(Integer id, Produto produto);
	
	List<Produto> findFilter(Produto filtro);
}