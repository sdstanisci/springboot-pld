package br.com.pld.transacoes.exception;

public class RegraNegocioException extends RuntimeException{
	
	public RegraNegocioException(String message) {
		super(message);
	}
}
