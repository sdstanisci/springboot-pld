package br.com.pld.transacoes.exception;

public class TransacaoNaoEncontradoException extends RuntimeException{
	
	public TransacaoNaoEncontradoException() {
		super("Transacao não encontrado !");
	}
}
