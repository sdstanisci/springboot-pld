package br.com.pld.transacoes.rest.controller;

import static org.springframework.http.HttpStatus.*;
import java.util.List;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import br.com.pld.transacoes.domain.entity.Produto;
import br.com.pld.transacoes.service.ProdutoService;

@RestController
@RequestMapping("/api/produto")
public class ProdutoController {

	private ProdutoService produtoService;

	public ProdutoController(ProdutoService produtoService) {
		this.produtoService = produtoService;
	}

	@GetMapping("{id}")
	public Produto getProdutoById(@PathVariable Integer id) {

		return produtoService.getProdutoById(id);
	}

	@PostMapping
	@ResponseStatus(CREATED)
	public Produto save(@RequestBody @Valid Produto produto) {

		return produtoService.save(produto);
	}

	@DeleteMapping("{id}")
	@ResponseStatus(NO_CONTENT)
	public void delete(@PathVariable Integer id) {

		produtoService.delete(id);
	}

	@PutMapping("{id}")
	@ResponseStatus(NO_CONTENT)
	public void update(@PathVariable Integer id, @RequestBody @Valid Produto produto) {

		produtoService.update(id, produto);
	}

	@GetMapping("filtro")
	public List<Produto> findFilter(Produto filtro) {

		return produtoService.findFilter(filtro);
	}
}
