package br.com.pld.transacoes.rest.controller;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NO_CONTENT;

import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.pld.transacoes.domain.entity.ItemTransacao;
import br.com.pld.transacoes.domain.entity.Transacao;
import br.com.pld.transacoes.domain.enums.StatusTransacao;
import br.com.pld.transacoes.rest.dto.AtualizacaoStatusTransacaoDTO;
import br.com.pld.transacoes.rest.dto.InformacoesItemTransacaoDTO;
import br.com.pld.transacoes.rest.dto.InformacoesTransacaoDTO;
import br.com.pld.transacoes.rest.dto.TransacaoDTO;
import br.com.pld.transacoes.service.TransacaoService;

@RestController
@RequestMapping("/api/transacao")
public class TransacaoController {

	private TransacaoService transacaoService;
	
	public TransacaoController(TransacaoService transacaoService) {
		this.transacaoService = transacaoService;
	}
	
	@PostMapping
	@ResponseStatus(CREATED)
	public Integer save(@RequestBody @Valid TransacaoDTO dto) {
		
		Transacao transacao = transacaoService.salvar(dto);
		return transacao.getId();
		
	}
	
	@PatchMapping("{id}")
	@ResponseStatus(NO_CONTENT)
	public void updateStatus(@PathVariable Integer id, @RequestBody AtualizacaoStatusTransacaoDTO dto) {
		
		String novoStatus = dto.getNovoStatus();
		transacaoService.atualizaStatus(id, StatusTransacao.valueOf(novoStatus));
	}
	
	@GetMapping("{id}")
	public InformacoesTransacaoDTO getById(@PathVariable Integer id) {
		
		return transacaoService
					.obterTransacaoCompleto(id)
					.map( transacao -> converter(transacao))
					.orElseThrow(()-> new ResponseStatusException( NOT_FOUND, "Transacao não encontrada !" ));
	}
	
	private InformacoesTransacaoDTO converter(Transacao transacao) {
		
		return InformacoesTransacaoDTO
			.builder()
			.dataTransacao(transacao.getDataTransacao().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
			.cpf(transacao.getCliente().getCpf())
			.nomeCliente(transacao.getCliente().getNome())
			.total(transacao.getTotal())
			.status(transacao.getStatus().name())
			.items(converter(transacao.getItens()))
			.build();
	}
	
	private List<InformacoesItemTransacaoDTO> converter (List<ItemTransacao> itens){
		
		if(CollectionUtils.isEmpty(itens))	
			return Collections.emptyList();
		
		return itens
				.stream()
				.map(item -> InformacoesItemTransacaoDTO
								.builder()
								.descricaoProduto(item.getProduto().getDescricao())
								.precoUnitario(item.getProduto().getPreco())
								.quantidade(item.getQuantidade())
								.build())
				.collect(Collectors.toList());
		
	}
}
