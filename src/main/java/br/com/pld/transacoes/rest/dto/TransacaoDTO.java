package br.com.pld.transacoes.rest.dto;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.constraints.NotNull;

import br.com.pld.transacoes.validation.NotEmptyList;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TransacaoDTO {

	@NotNull(message = "{campo.codigo-cliente.obrigatorio}")
	private Integer cliente;
	
	@NotNull(message = "{campo.total-transacao.obrigatorio}")
	private BigDecimal total;
	
	@NotEmptyList(message = "{campo.itens-transacao.obrigatorio}")
	private List<ItemTransacaoDTO> itens;
	
}
