package br.com.pld.transacoes.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ItemTransacaoDTO {

	private Integer produto;
	
	private Integer quantidade;
	
}
