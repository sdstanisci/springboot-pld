package br.com.pld.transacoes.rest.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.pld.transacoes.exception.TransacaoNaoEncontradoException;
import br.com.pld.transacoes.exception.RegraNegocioException;
import br.com.pld.transacoes.rest.ApiErros;

@RestControllerAdvice
public class ApplicationControllerAdvice {

	@ExceptionHandler(RegraNegocioException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ApiErros handleRegraNegocioException( RegraNegocioException ex) {
		
		String mensagemErro = ex.getMessage();
		
		return new ApiErros(mensagemErro);
	}
	
	@ExceptionHandler(TransacaoNaoEncontradoException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ApiErros handleTransacaoNotFoundException( TransacaoNaoEncontradoException ex) {
		
		return new ApiErros(ex.getMessage());
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ApiErros handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
		
		List<String> errors = ex.getBindingResult()
			.getAllErrors()
			.stream()
			.map( erro -> erro.getDefaultMessage())
			.collect(Collectors.toList());
		
		return new ApiErros(errors);
	}
}