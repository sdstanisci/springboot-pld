package br.com.pld.transacoes.rest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class AtualizacaoStatusTransacaoDTO {

	private String novoStatus;
}
