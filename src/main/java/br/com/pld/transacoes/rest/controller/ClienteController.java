package br.com.pld.transacoes.rest.controller;

import java.util.List;
import javax.validation.Valid;
import static org.springframework.http.HttpStatus.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import br.com.pld.transacoes.domain.entity.Cliente;
import br.com.pld.transacoes.service.ClienteService;

@RestController
@RequestMapping("/api/cliente")
public class ClienteController {

	private ClienteService clienteService;

	public ClienteController(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	@GetMapping("{id}")
	public Cliente getClienteById(@PathVariable Integer id) {

		return clienteService.getClienteById(id);
	}

	@PostMapping
	@ResponseStatus( CREATED )
	public Cliente save(@RequestBody @Valid Cliente cliente) {

		return clienteService.save(cliente);
	}

	@DeleteMapping("{id}")
	@ResponseStatus( NO_CONTENT )
	public void delete(@PathVariable Integer id) {

		clienteService.delete(id);
	}

	@PutMapping("{id}")
	@ResponseStatus( NO_CONTENT )
	public void update(@PathVariable Integer id, @RequestBody @Valid Cliente cliente) {
		
		clienteService.update(id, cliente);
	}
	
	@GetMapping("filtro")
	public List<Cliente> findFilter(Cliente filtro) {

		return clienteService.findFilter(filtro);
	}
}
