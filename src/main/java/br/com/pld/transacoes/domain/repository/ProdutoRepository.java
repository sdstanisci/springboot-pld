package br.com.pld.transacoes.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.pld.transacoes.domain.entity.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Integer>{

}
