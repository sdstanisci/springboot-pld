package br.com.pld.transacoes.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.pld.transacoes.domain.entity.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer>{

	List<Cliente> findByNomeLike(String nome);
	
	List<Cliente> findByNomeOrIdOrderById(String nome, Integer id);
	
	List<Cliente> findOneByNome(String nome);
	
	boolean existsByNome(String nome);
	
	@Query(value = "select c from Cliente c where c.nome like :nome")
	List<Cliente> encontrarNome(@Param("nome") String nome);
	
	@Query("select c from Cliente c left join fetch c.transacoes p where c.id = :id")
	Cliente findClienteFetchTransacao(Integer id);
		
}
