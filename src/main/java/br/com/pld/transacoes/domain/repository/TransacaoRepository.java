package br.com.pld.transacoes.domain.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.pld.transacoes.domain.entity.Cliente;
import br.com.pld.transacoes.domain.entity.Transacao;

public interface TransacaoRepository extends JpaRepository<Transacao, Integer> {

	List<Transacao> findByCliente(Cliente cliente);
	
	@Query("select t from Transacao t left join fetch t.itens where t.id = :id")
	Optional<Transacao> findByIdFetchItens(@Param("id") Integer id);
	
}
