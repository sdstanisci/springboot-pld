package br.com.pld.transacoes.domain.enums;

public enum StatusTransacao {
	
	REALIZADO,
	SUSPEITA;
}
