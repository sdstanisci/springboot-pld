package br.com.pld.transacoes.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.pld.transacoes.domain.entity.ItemTransacao;

public interface ItemTransacaoRepository extends JpaRepository<ItemTransacao, Integer> {

}
