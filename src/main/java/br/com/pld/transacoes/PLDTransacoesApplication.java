package br.com.pld.transacoes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PLDTransacoesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PLDTransacoesApplication.class, args);
	}

}
